<?php

/*
 * Class for setting up the plugin options...
 *
 * @since Doctoral_Training_Post_Types 0.4
 */
 
class Doctoral_Training_Post_Type_Options {

	 /*
	  * Add the options page in admin
	  */
	function add_menu() {
		add_options_page( 'DTC Options', 'DTC Options', 'manage_options', 'wdtc-plugin-options', array(&$this, 'options' ));
	}	
	
	 /*
	  * Set up the settings sections and fields
	  */
	function settings_setup() {

		// Add the sections
	
		add_settings_section(
			'wdtc_settings_section_profiles',
			'Profiles',
			array(&$this, 'settings_section_profiles'),
			'wdtc-plugin-options'
		);
	
		add_settings_section(
			'wdtc_settings_section_studentships',
			'Studentships',
			array(&$this, 'settings_section_studentships'),
			'wdtc-plugin-options'
		);
	
		add_settings_section(
			'wdtc_settings_section_pathways',
			'Pathways',
			array(&$this, 'settings_section_pathways'),
			'wdtc-plugin-options'
		);
	
		// Add the field
	
		add_settings_field(
			'wdtc_setting_profile_image_size',
			'Image size for profile pics',
			array(&$this, 'setting_profile_image_size'),
			'wdtc-plugin-options',
			'wdtc_settings_section_profiles'
		);
	
		add_settings_field(
			'wdtc_setting_path_to_studentships_page',
			'Path to studentships page',
			array(&$this, 'setting_path_to_studentships_page'),
			'wdtc-plugin-options',
			'wdtc_settings_section_studentships'
		);
	
		add_settings_field(
			'wdtc_setting_show_pathway_description_after_routes',
			'Pathway details',
			array(&$this, 'setting_show_pathway_description_after_routes'),
			'wdtc-plugin-options',
			'wdtc_settings_section_pathways'
		);
	
	
		// Register our setting so that $_POST handling is done for us and
		// our callback function just has to echo the <input>
		register_setting( 'wdtc-plugin-options', 'wdtc_setting_profile_image_size' );
		register_setting( 'wdtc-plugin-options', 'wdtc_setting_path_to_studentships_page' );
		register_setting( 'wdtc-plugin-options', 'wdtc_setting_show_pathway_description_after_routes' );
	}
	
	 /*
	  * Output html for the options page
	  */
	function options() {
		echo '<div class="wrap">';
		echo '	<h2>DTC Posts & Fields Options</h2>';
		echo '	<form action="options.php" method="POST">';
		settings_fields( 'wdtc-plugin-options' );
		do_settings_sections( 'wdtc-plugin-options' );
		submit_button();
		echo '	</form>';
		echo '</div>';
	}

	function settings_section_profiles() {
	}

	function settings_section_studentships() {
	}

	function settings_section_pathways() {
	}
	
	 /*
	  * The setting for the image size
	  */
	function setting_profile_image_size() {
		$image_sizes = get_intermediate_image_sizes();
		echo '<select name="wdtc_setting_profile_image_size">';
		foreach ($image_sizes as $size_name) {
			echo '<option value="' . $size_name . '"';
			if ($size_name == get_option('wdtc_setting_profile_image_size'))
				echo 'selected';
			echo '>' . $size_name . '</option>';
		}
		echo '</select>';
		echo '<p class="description">This will not apply to profiles displayed on single profile pages, full profile archives and the random profile widget which are the default thumbnail size</p>';
	}

	 /*
	  * The path to the main list of studentships
	  */
	function setting_path_to_studentships_page() {
		echo '<input name="wdtc_setting_path_to_studentships_page" id="wdtc_setting_path_to_studentships_page" type="text" class="text" value="' . get_option( 'wdtc_setting_path_to_studentships_page' ) . '" />';
		echo '<p class="description">Relative path to the page where the full list of studentships is advertised (eg. applications/studentships)</p>';
	}

	 /*
	  * The checkbox for whether to show pathway details individually for each institution offering a pathway
	  */
	function setting_show_pathway_description_after_routes() {
		echo '<input name="wdtc_setting_show_pathway_description_after_routes" id="wdtc_setting_show_pathway_description_after_routes" type="checkbox" value="1" class="code" ' . checked( 1, get_option( 'wdtc_setting_show_pathway_description_after_routes' ), false ) . ' />';
		echo '<p class="description">Show the pathway description after the pathway details (rather than showing it first)</p>';
	}
}