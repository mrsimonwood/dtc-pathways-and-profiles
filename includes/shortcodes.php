<?php

/*
 * Class for displaying profile
 *
 * @since Doctoral Training 0.1
 */
class WDTC_Profile_Displayer extends WDTC_Shortcode_Handler {

	/*
	 * Sets the parameters and the defaults for the shortcode
	 *
	 */
	protected function get_pairs() {
		return array( 'start_before' => '', 'start_after' => '', 'end_before' => '', 'end_after' => '', 'status' => 'all', 'archived' => false );
	}

	/*
	 * Call display profiles
	 *
	 * @param array $atts the attributes provided along with the shortcode
	 *
	 * @return string html to display the profiles
	 */
	protected function shortcode_output($atts) {
		$start_before = strtotime($atts['start_before']);
		$start_after = strtotime($atts['start_after']);
		$end_before = strtotime($atts['end_before']);
		$end_after = strtotime($atts['end_after']);
		$status = $atts['status'];
		$archived = $atts['archived'];
		$args = $this->query_args($start_before, $start_after, $end_before, $end_after, $status, $archived);
		return $this->display_profiles($args);
	}

	/*
	 * Generate an array for a WP_Query meta query to find posts before/after/between values
	 *
	 * @param int $before the highest value to query for
	 * @param int $after the lowest value to query for
	 * @param string $key custom field value
	 * @param bool $includeblanks
	 *
	 * @return array for meta query
	 */
	function meta_query_comparison($before, $after, $key, $includeblanks = false)
	{
		$compare = array();
		if ($before || $after) {
			$compare['key']	 		= $key;
			if ($before && $after) {
				$compare['value']   = array ($after, $before);
				$compare['compare'] = 'BETWEEN';
			} elseif($before) {
				$compare['value']   = $before;
				$compare['compare'] = '<=';
			} else {
				$compare['value']   = $after;
				$compare['compare'] = '>=';
			}
		}
		if ($includeblanks)
			$compare = array('relation' => 'OR', $compare, array('key' => $key, 'compare' => 'NOT EXISTS'));
		return $compare;
	}

	/*
	 * Generate the arguments for a WP_Query to select the profiles we want
	 *
	 * @param int $start_before the 
	 * @param int $start_after the time 
	 * @param int $end_before the time 
	 * @param int $end_after the time 
	 * @param int $status the time 
	 *
	 * @return array the arguments for the query
	 */
	private function query_args($start_before, $start_after, $end_before, $end_after, $status, $archived) {
		$args = array(
			'post_type'			=>	'profile',
			'posts_per_page'	=>	'-1',
			'orderby'			=>	'title',
			'order'				=>	'ASC'
		);
		if ($archived) {
			$args['post_status'] = 'archive';
		} else {
			$args['post_status'] = 'publish';
		}
		$args['meta_query'][] = $this->meta_query_comparison($start_before, $start_after, 'wdtc-start-date');
		$args['meta_query'][] = $this->meta_query_comparison($end_before, $end_after, 'wdtc-end-date');
		if ($status == 'current')
		{
			$args['meta_query'][] = $this->meta_query_comparison(NULL, time(), 'wdtc-end-date', true);
		} elseif ($status == 'graduated')
		{
			$args['meta_query'][] = $this->meta_query_comparison(time(), NULL , 'wdtc-end-date');	
		}
		return $args;
	}
	
	/*
	 * Get profiles via WP_Query and generate html to display them
	 *
	 * @param array $args args for the WP_Query
	 *
	 * @return string html do display the profiles
	 */
	private function display_profiles($args) {
//		$customiser = new WDTC_Post_Type_Customiser('profile', array('hide_content'=>true, 'custom_thumb_size'=>true,'placeholder_thumnail'=>true, 'classes'=>array('short'),  'apply_in_taxonomy_archive'=>false));
//		$customiser->init();
		global $wp_query;
		$main_query = $wp_query;
		$wp_query = new WP_Query( $args );
 	    // Reset and setup variables
		$html = '';
		$temp_title = '';
		$temp_link = '';

		// the loop
		if ($wp_query->have_posts()) {
			$html .= '<div class="profiles shortcode">';
			ob_start();
	
			while ($wp_query->have_posts()) {
				$wp_query->the_post();
				get_template_part( 'content', get_post_format() );
			}
			$html .= ob_get_contents();
			ob_end_clean();
			$html .= '</div>';
		} else {
			  $html .= __('Nothing found.','dtc-pathways-and-profiles');
		}   
//		$customiser->remove();
// 		wp_reset_query();
		$wp_query = $main_query;
		return $html;
	}
}

/*
 * Class for displaying a list of studentships
 *
 * @since Doctoral Training 0.1
 */
class WDTC_Studentship_Lister extends WDTC_Shortcode_Handler {

//	var $thingy;

//	function __construct($thingy) {
//		$this->thingy = $thingy;
//	} 

	/*
	 * Sets the parameters and the defaults for the shortcode
	 *
	 */
	protected function get_pairs() {
		return array('group' => '', 'style' => 'sublists', 'hide_application_date' => 'false', 'show_multiple_as_up_to' => 'false');
	}
	
	/*
	 * Get studentships and generate list
	 *
	 * @param array $atts the attributes provided along with the shortcode
	 *
	 * @return string html to display the studentships
	 */
	protected function shortcode_output($atts) {
		$studentships = get_posts( array('post_type' => 'studentship', 'group' => $atts[group], 'order'=> 'ASC', 'orderby' => 'title', 'posts_per_page' => -1, 'post_status' => array('publish','archive'), 'suppress_filters' => 0) );
		$sublists = false;
		if ($atts['style'] == 'sublists')
			$sublists = true;
		$hide_application_date = false;
		if ($atts['hide_application_date'] == 'true')
			$hide_application_date = true;
		if ($atts['show_multiple_as_up_to'] == 'true')
			$show_multiple_as_up_to = true;
		$comma_separated = false;
		if ($atts['style'] == 'comma separated')
			$comma_separated = true;
		return $this->list_studentships($studentships, $sublists, $comma_separated, $hide_application_date, $show_multiple_as_up_to);
	}

	/*
	 * Generate html to list the studentships
	 *
	 * @param array $studentships array of the studentship posts to display
	 * @param bool $sublists
	 * @param bool $comma_separated
	 *
	 * @return string html to display the studentships
	 */	
	private function list_studentships($studentships, $sublists = false, $comma_separated = false, $hide_application_date = false, $show_multiple_as_up_to = false) {
		$html = '<ul>';	
		foreach($studentships as $studentship) {	
			$new_pathways = get_the_term_list( $studentship->ID, 'pathway', '',', ','' );
			if (($new_pathways != $prev_pathways))
			{
				if ($prev_pathways)
				{
					$html .= '</li>';
					if ($sublists)
						$html .='</ul>';
				}
				$html .= '<li>' . $new_pathways . ': ';
				$prev_pathways = $new_pathways;
				$first = true;
				if ($sublists)
					$html .='<ul>';
			}
			if ($sublists) {
				$html .= '<li>';
			} elseif ($commaseparated) {
				if (!$first)
					$html .= ', ';
			}
			$html .= $this->display_studentship($studentship, true, $hide_application_date, $show_multiple_as_up_to);
			$first = false;
			if ($sublists)
				$html .= '</li>';
		}
		if ($sublists)
			$html .='</ul>';
		$html .= '</li></ul>';
		return $html;		
	}
	
	/*
	 * Display the details of a studentship
	 *
	 * @param WP_Post $studentship the studentship post object
	 * @param bool $inalists whether the studentship is being displayed within a bullet list
	 *
	 * @return string html to display the studentships
	 */
	private function display_studentship ( $studentship, $inalist = false, $hide_application_date = false, $show_multiple_as_up_to = false ) {
		global $prev_institutions;
		$new_institutions = WDTC_Term_Lists::get_the_term_ancestors_list( $studentship->ID, 'institution', '', ', ', '', array('show'=>false), false);
		$prev_institutions = $new_institutions;
		$studentship_url = get_post_meta( $studentship->ID, 'wdtc-studentship-url', true );
		$studentship_quantity = get_post_meta( $studentship->ID, 'wdtc-studentship-quantity', true );
		$application_date = get_post_meta( $studentship->ID, 'wdtc-application-date', true );
		$studentship_type = get_post_meta( $studentship->ID, 'wdtc-studentship-type', true );
		if ($studentship_type == 'named')
			$studentshipname = '"' . get_post_meta( $studentship->ID, 'wdtc-studentship-title', true ) . '"';
		if ($studentship_type != 'named' ||  $studentship_quantity > 1 || $inalist) {
			$studentship_type_details = new WDTC_Studentship_Type_Details;
			$studentshipnumbers = $studentship_quantity . ' ' . $studentship_type_details->studentship_type_name($studentship_type,$studentship_quantity,true);
			if ($show_multiple_as_up_to && $studentship_quantity > 1)
				$studentshipnumbers = __('Up to ', 'dtc-pathways-and-profiles') . $studentshipnumbers;
		}
		if ($application_date > time() - 24 * 60 * 60 && $studentship_url) {
			if ($studentshipname) {
				$studentshipname = WDTC_String_Formatter::linkify($studentship_url, $studentshipname);
			} elseif ($studentshipnumbers) {
				$studentshipnumbers = WDTC_String_Formatter::linkify($studentship_url, $studentshipnumbers);
			}
		}
		$html = $studentshipnumbers . ' ' . $studentshipname;
		if ($inalist) {
			$html .=' - ' . $new_institutions;
		} elseif (!$hide_application_date) {
			$html .= ' (';
		}
		if ($application_date && !$hide_application_date)
		{
			if ($inalist)
				$html .= ' (';
			if ($application_date > time() - 24 * 60 * 60)
			{
				$html .= __('apply by ', 'dtc-pathways-and-profiles') . date('D d M Y',$application_date);
			} else {
				$html .= __('applications closed ', 'dtc-pathways-and-profiles') . date('D d M Y',$application_date);
			}
			if ($inalist)
				$html .= ')';
		} else {
			if (!$inalist && !$hide_application_date)
				$html  .= __('application date TBC', 'dtc-pathways-and-profiles');
		}
		$editurl = get_edit_post_link($studentship->ID);
		if ($editurl) {
			$html .= ' <a href="'. $editurl . '">(Edit)</a>';
		}
		return $html;
	}
}