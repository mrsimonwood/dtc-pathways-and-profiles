<?php

/*
 * Class for providing the facility on user profile pages for administrators to
 * match the user with a profile post.
 *
 * @since DTC Pathways and Profiles 0.1.7
 */

class WDTC_Profile_Matcher {

    public function init() {
       // actions to add the facility to select a profile post from a user profile page:
       add_action('show_user_profile', array(&$this, 'profile_match_field' ));
       add_action('edit_user_profile', array(&$this, 'profile_match_field' ));
       add_action('personal_options_update', array(&$this, 'save_profile_match_field' ));
       add_action('edit_user_profile_update', array(&$this, 'save_profile_match_field' ));
    }  
        
    public function profile_match_field( $user ) {
        $user_id = $user->ID;
        if ( current_user_can( 'promote_users', $user_id ) ) {
            echo '<h3>Profile Post</h3>';
			echo '<table class="form-table">';
			echo '	<tr>';
			echo '		<th><label for="dropdown">Profile Post</label></th>';
			echo '		<td>';
			$selected = get_user_meta( $user_id, 'profile_id', true );
			echo '			<select name="profile_id" id="profile_id">';			
			echo '				<option value="">';
		    echo '[Select Profile Post]';
		    echo '</option>';
			$query = new WP_Query(array(
		    'post_type' => 'profile',
		    'post_status' => 'publish',
		    'posts_per_page' => '-1',
		    'orderby' => 'title',
		    'order' => 'ASC'
			));
			while ($query->have_posts()) {
			    $query->the_post();
			    $post_id = get_the_ID();

				echo '				<option value="' . $post_id . '" ';
				echo ($selected == $post_id) ?  'selected="selected"' : '';	
				echo '>';		
			    the_title();
			    echo '</option>';
			}
			wp_reset_query();				
			echo '			</select>';
			echo '		</td>';
			echo '	</tr>';
			echo '</table>';
        }
    }
    
    public function save_profile_match_field( $user_id ) {
        if ( !current_user_can( 'promote_users', $user_id ) )
            return false;
        $profile_id = $_POST['profile_id'];
        update_usermeta( $user_id, 'profile_id', $profile_id ); 					// update the user with the id of their profile post
        wp_update_post( array( 'ID' => $profile_id, 'post_author' => $user_id ));	// update the profile post to make the user the author
    }
}