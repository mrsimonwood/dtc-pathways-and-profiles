<?php

/*
 * Class for the profile form
 *
 * @since Doctoral Training 0.1
 */
class WDTC_Edit_Profile_Form extends WDTC_Edit_Post_Form {

	/*
	 * Array of additional fields to appear before the parent fields
	 *
	 */
	private $additional_pre_fields;
	
	/*
	 * Set up action hooks
	 */
	public function init() {
		parent::init();
		add_action('personal_options', array(&$this, 'profile_link_in_admin'));
		add_filter('wpmu_users_columns' , array(&$this, 'add_profile_column_in_admin_users_list'));
		add_action('manage_users_custom_column', array(&$this, 'profile_column_in_admin_users_list_content'), 10, 3);
	}
	
	/*
	 * Add pre field
	 *
	 * @param WDTC_Attribute field to add
	 */
	private function add_pre_field(WDTC_Attribute $field) {
		$this->additional_pre_fields[] = $field;
	}
		
	/*
	 * Prepend additional fields to array of fields
	 *
	 * @param array WDTC_Attribute fields to which the additional fields should be prepended
	 *
	 * @return array of WDTC_Attribute fields
	 */
	public function prepend_additional_fields($fields) {
		return array_merge($this->additional_pre_fields, $fields);
	}
	
	/*
	 * Type of post the form will edit is 'profile' CPT.
	 *
	 * @return string post type
	 */	
	protected function get_type() {
		return 'profile';
	}
	
	/*
	 * Customised wording for login request shown to non-logged in users
	 *
	 * @return string login message
	 */
	protected function login_message() {
		return "To edit your profile, first sign in";
	}
	
	/*
	 * Name to use for the 'field' that is actually the post content
	 *
	 * @return string name for post content
	 */
	protected function content_field_name() {
		return 'Project Outline';
	}

	/*
	 * Description to show with the editor for the post content
	 *
	 * @return string description for post content
	 */
	protected function content_description() {
		return '<strong>' . $this->content_field_name() . '</strong><br/>Between 100 & 200 words – include aims or research questions if you wish.';
	}

	/*
	 * Before the other fields, display a field for name
	 *
	 * @return string html to display before other fields
	 */
	protected function before_fields() {
        $current_user = wp_get_current_user();
		if (class_exists('WDTC_Custom_Text_Field')) {
			$field = new WDTC_Custom_Text_Field('user_name','Name',array('validation_pattern'=>'alpha_num_standard'));
			$this->add_pre_field($field);
			add_filter('wdtc_validate_fields_setup', array(&$this, 'prepend_additional_fields'));
			add_filter('wdtc_validate_fields', array(&$this, 'prepend_additional_fields'));
			$value = $current_user->user_firstname . ' ' . $current_user->user_lastname;
			if($_POST['user_name'])
				$value = $_POST['user_name'];
			return $field->get_form_field($this->get_post_to_edit_id(),$value);
		}
		return;
	}

	/*
	 * Get the html for the form field for the post title
	 *
	 * @return string html
	 */	
	protected function title_field () {
		return '';
	}
	
	/*
	 * Value to save as the post title
	 *
	 * @return string post title
	 */
	protected function title_value() {
		return WDTC_String_Formatter::formatname($_POST['user_name'],false);
	}
	
	/*
	 * At the point when the form is saved, we need to save the name field
	 * and copy the email field to the user's own data
	 */
	protected function additional_stuff_when_form_saved($post_id, $user_id) {
		$user_data=array();
		$user_data['ID'] = $user_id;
		// Save the name field if it has been completed by a logged in user
		$user_name = $_POST['user_name'];
		if (!empty($user_name))
		{
			$user_name = wp_kses_data($user_name);
			if( is_numeric($user_id) ) {
				$user_data['display_name'] = $user_name;
				$user_name = explode(' ', $user_name);
				$user_data['last_name'] = array_pop($user_name);
				$user_data['first_name'] = implode(' ',$user_name);
			}
		}
		// Save the Wordpress email field
		if (!empty($_POST['wdtc-email'])) {
			if( is_numeric($user_id) ) {
				$user_data['user_email'] = $_POST['wdtc-email'];
			}
		}
		if (count($user_data) > 1)
				$ud = wp_update_user($user_data);
	}

	/*
	 * The post to edit is the profile for the currently logged in user
	 *
	 * @return int post id for profile of current user
	 */
	protected function get_post_to_edit_id() {
		return $this->get_user_profile_id();
	}
	
	/*
	 * The source id for fields is the post id for the profile for the currently
	 * logged in user
	 *
	 * @return int post id for profile of current user
	 */
	protected function source_id_for_field() {
		return $this->get_user_profile_id();
	}
	
	/*
	 * Create a new profile post, also record the id for this post in the currently
	 * logged in user's metadata
	 *
	 * @return int post id for the newly created post
	 */
	protected function insert_post ($post_info, $user_id) {
		$post_id = parent::insert_post ($post_info, $user_id);
		update_user_meta($user_id, 'profile_id', $post_id);
		return $post_id;
	}
	
	/*
	 * Get the id of the post which is the profile for the given user
	 *
	 * @return int id of the profile post
	 */	
	private function get_user_profile_id($user_id = NULL) {
        if (!$user_id) {
	        $current_user = wp_get_current_user();
			$user_id = $current_user->ID;
		}
		$profileid = get_user_meta( $user_id, 'profile_id', true);
		if (get_post_status($profileid) == 'trash') {
			$profile = get_post($profileid);
			$profileid = $profile->post_parent;
			if (get_post_status($profileid) == 'trash')
				return null;
		}
		return $profileid;
	}

	/*
	 * Show links to view and edit a user's public profile on their (WP admin) profile page
	 *
	 * @param WP_User $user the user
	 */	
	public function profile_link_in_admin($user) {
		$post_id = $this->get_user_profile_id($user->ID);
		if ($post_id) {
			$html = '<th><label for="persistent_id">Student Profile</label></th>';
			$html .= '	<td>'; 
			$html .=  		'<a href="' . get_permalink($post_id) . '">View Student Profile</a>';
			if (current_user_can('edit_profile'))
				$html .= ' | <a href="' . get_edit_post_link($post_id) . '">Edit Student Profile</a>';
			$html .= '	</td>';
			$html .= '</tr>';
			echo $html;
		}
	}

	/*
	 * Add a column to the users listing in admin for users' profiles
	 *
	 * @param array $columns the columns in the listing table
	 *
	 * @return array amended array of columns in the listing table
	 */	
	public function add_profile_column_in_admin_users_list($columns) {
    	return array_merge( $columns, array('profile_id' => __('Profile','dtc-pathways-and-profiles')) );
	}

	/*
	 * For each user in the users listing in admin show whether they have a profile page
	 * (if yes, hyperlink to it)
	 *
	 * @param string $value existing content for column entries
	 * @param string $column_name the column id for the column entry
	 * @param int $user_id the id of the user for the row
	 *
	 * @return string content for the column entry
	 */	
	public function profile_column_in_admin_users_list_content( $value, $column_name, $user_id ) {
		if ($column_name == 'profile_id') {
			$profileid = $this->get_user_profile_id( $user_id);
			if (!$profileid)
				return 'No';
			return '<a href="' . get_permalink($profileid) . '">Yes</a>';
		}
		return $value;
	}
	
}