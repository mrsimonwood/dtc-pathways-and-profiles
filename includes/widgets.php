<?php

/* 
 * Display a random profile in a widget 
 */
class Featured_Profile_Widget extends WP_Widget
{
	/*
	 * Constructor
	 */
	function __construct() {
		$widget_ops = array(
			'classname' => 'featured_profile_widget',
			'description' => 'Display a random profile'
		);
		parent::__construct(
			'Featured_Profile_Widget',
			'Featured Profile',
			$widget_ops
		);
	}
	
	/*
	 * Outpot the widget html
	 *
	 * @param array $args
	 * @param array $instance the widget options
	 */
	function widget($args, $instance) { // widget sidebar output
		extract($args, EXTR_SKIP);
		echo $before_widget; // pre-widget code from theme
		echo '<h3 class="widget-title">' . __('Featured Profile','dtc-pathways-and-profiles') . '</h3>';
		$args = array( 'post_type'=> 'profile', 'numberposts' => 1, 'orderby' => 'rand' );
		$featured_profiles = get_posts( $args );
		$taxonomy = 'pathway';
		foreach( $featured_profiles as $featured ){ 	
			$profile_url = get_permalink( $featured->ID );
			echo '<ul>';
			echo '<li><a href="' . $profile_url . '">' . get_the_post_thumbnail($featured->ID, 'thumbnail', array('class' => 'alignleft')) . '</a>';
			echo '<h4><a href="' . $profile_url . '">' . WDTC_String_Formatter::formatname($featured->post_title) . '</a></h4>';
			echo '<p>' . get_post_meta($featured->ID, 'wdtc-research-topic', true) . '</p>';
			echo '</li>';
			echo '</ul>';
			echo '<p class="see-more"><a href="profiles">' . __('More profiles...','dtc-pathways-and-profiles') . '</a></p>';
		}
		echo $after_widget; // post-widget code from theme
	}
}

/*
 * A widget to display studentships that are open for applications.
 */
class Studentships_Widget extends WP_Widget
{
	/*
	 * Constructor
	 */
	function __construct() {
		$widget_ops = array(
			'classname' => 'studentships_widget',
			'description' => 'Available Studentships'
		);
		parent::__construct(
			'Studentships_Widget',
			'Studentships',
			$widget_ops
		);
	}
	
	/*
	 * Outpot the widget html
	 *
	 * @param array $args
	 * @param array $instance the widget options
	 */
	function widget($args, $instance) { // widget sidebar output
		extract($args, EXTR_SKIP);
        $args = array( 'post_type'=> 'studentship', 'numberposts' => $instance['number_of_studentships'], 'meta_key' => 'wdtc-application-date', 'orderby' => 'meta_value', 'order' => 'ASC', 'meta_value' => time(), 'meta_compare' => '>');
        $studentships = get_posts( $args );
		if ($studentships) {
			echo $before_widget; // pre-widget code from theme
			echo '<h3 class="widget-title">' . __('Studentships','dtc-pathways-and-profiles') . '</h3>';
			foreach( $studentships as $studentship ){
				$studentship_url = get_post_meta( $studentship->ID, 'wdtc-studentship-url', true );
				$studentship_quantity = get_post_meta( $studentship->ID, 'wdtc-studentship-quantity', true );
				$application_date = get_post_meta( $studentship->ID, 'wdtc-application-date', true );
				$studentship_type = get_post_meta( $studentship->ID, 'wdtc-studentship-type', true );
				$pathways = get_the_terms($studentship->ID, 'pathway');
				if ($pathways) {
					echo '<ul>';
					foreach ($pathways as $pathway) {
						echo '<li class="studentship-widget">';					
						if ($studentship_url)
							echo '<a href="' . $studentship_url . '">';
						echo $studentship_quantity . ' ' . $studentship_type . ' ' . _n('studentship', 'studentships', $studentship_quantity);
						echo ' in ' . $pathway->name;
						if ($studentship_url)
							echo '</a>';
						echo ' at ' . WDTC_Term_Lists::get_the_term_ancestors_list( $studentship->ID, 'institution', '',', ','',  array ('show' => false), false ) . ' ';
						echo '<div class="studentship-widget-meta">apply by ' . date('D d M Y',$application_date) . '</div>';
						echo '</li>';
					}
					echo '</ul>';
				}
			}
			$studentships_path = get_option('wdtc_setting_path_to_studentships_page');
			if ($instance['show_studentships_path'] && get_option('wdtc_setting_path_to_studentships_page'))
				echo '<p class="see-more"><a href="' . get_site_url() . '/' . $studentships_path . '">See all studentships...</a></p>';
			echo $after_widget; // post-widget code from theme
		}
	}
	
	/*
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		//Strip tags from title and name to remove HTML 
		$instance['show_studentships_path'] = $new_instance['show_studentships_path'];
		$instance['number_of_studentships'] = $new_instance['number_of_studentships'];
		return $instance;
	}

	/*
	 * Output the options form in admin
	 *
	 * @param array $instance widget options
	 */
	function form( $instance ) {
		//Set up some default widget settings.
		$defaults = array( 'show_studentships_path' => '', 'number_of_studentships' => 3 );
		$instance = wp_parse_args( (array) $instance, $defaults ); 
		echo '<p>Show up to <select name="' . $this->get_field_name( 'number_of_studentships' ) . '">';
		for ($i = 2; $i <= 10; $i++)
		{
			echo '<option value="' . $i . '"';
			if ($i == $instance['number_of_studentships'])
				echo ' selected';
			echo '>' . $i . '</option>';
		}
		echo '</select> studentships</p>';
		$studentships_path = get_option('wdtc_setting_path_to_studentships_page');
		if ($studentships_path)
		{
			echo '<p><input class="checkbox" type="checkbox" id="' . $this->get_field_id( 'show_studentships_path' ) . '" name="' . $this->get_field_name( 'show_studentships_path' ) . '"';
			if ($instance['show_studentships_path'])
				echo ' checked';
			echo ' />';
			echo '<label for="' . $this->get_field_id( 'show_studentships_path' ) . '">' . __(' Show a link to /', 'dtc-pathways-and-profiles') . $studentships_path .  ' </label></p>';
		}
	}
}
