<?php
/*
 * DTP Pathways and Profiles
 *
 * @package   DTC_Pathways_and_Profiles
 * @author    Simon Wood <wordpress@simonwood.info>
 * @license   GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: DTP Pathways and Profiles
Bitbucket Plugin URI: https://bitbucket.org/mrsimonwood/dtc-pathways-and-profiles
 * Description: Provides post types, fields and taxonomies relevant to a Doctoral Training Partnership . Depends on the DTP Posts and Fields plugin, requires DTP Frontend Editor to edit profiles.
 * Version: 	0.1.17
 * Author: 		Simon Wood
 * Author URI: 	http://simonwood.info
 * Text Domain:	dtc-pathways-and-profiles
 * License: 	GNU General Public License v2.0
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 */

function dtc_pathways_and_profiles_init() {
	$plugin = new WDTC_Profiles_and_Pathways;
	$plugin->init();
}
add_action('dtc_posts_and_fields_init', 'dtc_pathways_and_profiles_init', 15);

/* Rough and ready fix for post ordering issues... */

function posts_orderby_post_type ( $orderby ){
    global $wpdb;
    if(!is_admin() && (is_tax() || is_post_type_archive())) 
        $orderby =  $wpdb->prefix."posts.post_type DESC, {$wpdb->prefix}posts.post_title ASC";
    return  $orderby;
}
add_filter('posts_orderby','posts_orderby_post_type');

remove_filter('pre_term_description', 'wp_filter_kses');

/*
 * Plugin class
 *
 * @since Doctoral Training 0.1
 */
class WDTC_Profiles_and_Pathways {

	/*
	 * Initialise the plugin setting up options, taxonomies, post types, shortcodes and hooks
	 *
	 */
	public function init() {
		$this->setup_options();
		foreach($this->taxonomy_details() as $key => $tax) {
			$tax_customisations = (isset($tax['customisations']) ? $tax['customisations'] : NULL);
			$registerer = new WDTC_Taxonomy_Registerer($key, $tax['name'], $tax['singular_name'], $tax['hierarchical'], $tax['post_types'], $tax_customisations);
			$registerer->init();
		}
		add_action('wdtc_profile_fields', array(&$this, 'setup_profile_form'));
		$initialiser = WDTC_Data_Field_Initialiser::Instance();	
		$initialiser->add_data_handler('post_meta_data', new WDTC_Post_Meta_Data);
		$initialiser->add_data_handler('hierarchical_tax_data', new WDTC_Hierarchical_Tax_Data);
		$initialiser->add_data_handler('nonhierarchical_tax_data', new WDTC_Nonhierarchical_Tax_Data);
		foreach ($this->post_type_details() as $key => $posttype) {
			$field_data = (isset($posttype['field_data']) ? $posttype['field_data'] : array());
			$pt_customisations = (isset($posttype['customisations']) ? $posttype['customisations'] : NULL);
			$registerer = new WDTC_Post_Type_Registerer($key, $posttype['name'], $posttype['singular_name'], $posttype['supports'],$field_data,$pt_customisations);
			$registerer->init();
		}
		
		$hider = new WDTC_Display_With_Content_Hidden;
		$hider->add_context('profile', array('institution', 'pathway', 'topic'), array('institution'), true);
		$hider->activate_hooks(true);

		$thumb_sizer = new WDTC_Display_With_Custom_Thumb_Size;
		$thumb_sizer->add_context('profile', array('institution', 'pathway', 'topic'), array('institution'), true);
		$thumb_sizer->activate_hooks(true);

		$title_formatter = new WDTC_Display_With_Formatted_Name_In_Title;
		$title_formatter->add_context('profile', array('institution', 'pathway', 'topic'), array('institution'), true);
		$title_formatter->activate_hooks(true);

		$class_supplementer = new WDTC_Display_With_Additional_Classes;
		$class_supplementer->add_context('profile', array('institution', 'pathway', 'topic'), array('institution'), true);
		$class_supplementer->set_classes(array('short'));
		$class_supplementer->activate_hooks(true);

		$class_supplementer = new WDTC_Display_With_Additional_Classes;
		$class_supplementer->add_context('route', array('institution', 'pathway'), array(), false);
		$class_supplementer->set_classes(array('highlight'));
		$class_supplementer->activate_hooks(true);

		if (get_option( 'wdtc_setting_show_pathway_description_after_routes' )) {
			$class_supplementer = new WDTC_Display_With_Additional_Classes;
			$class_supplementer->add_context('route', array('pathway'), array(), false);
			$class_supplementer->set_classes(array('third-box'));
			$class_supplementer->activate_hooks(true);
		}

		$field_hider = new WDTC_Display_With_Fields_Hidden;
		$field_hider->add_context('route', array(), array(), true);
		$field_hider->set_fields(array('wdtc-contact', 'pathway'));
		$field_hider->activate_hooks(true);

		$field_hider = new WDTC_Display_With_Fields_Hidden;
		$field_hider->add_context('route', array('institution'), array(), false);
		$field_hider->set_fields(array('institution', 'wdtc-contact'));
		$field_hider->activate_hooks(true);

		$field_hider = new WDTC_Display_With_Fields_Hidden;
		$field_hider->add_context('studentship', array('pathway', 'institution'), array(), true);
		$field_hider->set_fields(array('group'));
		$field_hider->activate_hooks(true);

		$field_hider = new WDTC_Display_With_Fields_Hidden;
		$field_hider->add_context('studentship', array(), array(), true);
		$field_hider->set_fields(array('group', 'pathway'));
		$field_hider->activate_hooks(true);

		$subhead_displayer = new WDTC_Display_With_Subhead;
		$subhead_displayer->set_contexts(array('route', 'studentship', 'institution', 'pathway'));
		$subhead_displayer->set_subhead_taxonomy('pathway');
		$subhead_displayer->activate_hooks(true);

		$archive_page_title_modifier = new WDTC_Display_With_Modified_Archive_Page_Title;
		$archive_page_title_modifier->set_contexts(array('profile', 'route', 'studentship', 'institution', 'pathway'));
		$archive_page_title_modifier->activate_hooks(true);

		$archive_page_title_modifier2 = new WDTC_Display_With_Modified_Archive_Page_Title_With_Tax_Name;
		$archive_page_title_modifier2->set_contexts(array('topic'));
		$archive_page_title_modifier2->activate_hooks(true);

		$term_displayer = new WDTC_Display_Taxonomy_Archive_With_Term_Description;
		$term_displayer->set_contexts(array('institution'));
		$term_displayer->activate_hooks(true);

		$term_displayer = new WDTC_Display_Taxonomy_Archive_With_Term_Description;
		$term_displayer->set_contexts(array('pathway'));
		if (get_option( 'wdtc_setting_show_pathway_description_after_routes' ))
			$term_displayer->set_post_type_to_display_term_description_after('route');
		$term_displayer->activate_hooks(true);

		$post_type_orderer = new WDTC_Display_By_Post_Type_In_Archive;
		$post_type_orderer->set_contexts(array('pathway', 'institution'));
		$post_type_orderer->activate_hooks(true);

		
		$this->auto_titlers(array('route' => array('pathway', 'institution'), 'studentship' => array('pathway','institution','group')));
		$this->widgets();
		$this->shortcodes();
		/* Custom URLs */
		// add_action('admin_init', 'flush_rewrite_rules');
		add_action('generate_rewrite_rules', array(&$this, 'profiles_rewrite_rules'));
		// add_action('generate_rewrite_rules', array(&$this, 'routes_rewrite_rules'));
		// Better place to put this?:
		add_action( 'pre_get_posts', array(&$this, 'no_limit_posts'));
		$archive_registerer = new WDTC_Post_Status_Registerer('archive', 'Archive', 'Archived', array('profile', 'studentship'));
		$archive_registerer->init();
		do_action( 'doctoral_training_init' );
		add_filter('wdtc_custom_thumb_size', array(&$this, 'profile_image_size'));
		add_filter('wdtc_custom_thumb_default', array(&$this, 'profile_image_default'));
		
		$this->setup_profile_matcher();
	}
	
	/*
	 * Setup the options
	 *
	 */
	private function setup_options() {
		require_once dirname( __FILE__ ) . '/admin/options.php';
		$options = new Doctoral_Training_Post_Type_Options();
		add_action('admin_menu', array($options, 'add_menu'));
		add_action('admin_init', array($options, 'settings_setup'));
	}

	/*
	 * Define the custom taxonomies to be registered
	 *
	 * @return array array containing the arguments for each new taxonomy and the post
	 * types they should apply to
	 */
	private function taxonomy_details() {
		$wtccustomisations = array('subhead'=>true, 'term_description_displayed'=>true, 'order_by_post_type'=>true, 'modify_archive_page_title'=>true);  // array to use for pathway customisation
		if (get_option( 'wdtc_setting_show_pathway_description_after_routes' ))
			$wtccustomisations['term_description_after_post_type'] = 'route';
		return array(
			'institution' => array(
				'name' => __('Institutions','dtc-pathways-and-profiles'),
				'singular_name' => __('Institution','dtc-pathways-and-profiles'),
				'hierarchical' => true,
				'post_types' => array ('profile', 'route', 'studentship')
			),
			'pathway' => array(
				'name' => __('Pathways','dtc-pathways-and-profiles'),
				'singular_name' => __('Pathway','dtc-pathways-and-profiles'),
				'hierarchical' => true,
				'post_types' => array ('profile', 'route', 'studentship')
			),
			'structure' => array(
				'name' => __('Structures of Award','dtc-pathways-and-profiles'),
				'singular_name' => __('Structure of Award','dtc-pathways-and-profiles'),
				'hierarchical' => true,
				'post_types' => array ('route')
			),
			'mode' => array(
				'name' => __('Modes of Study','dtc-pathways-and-profiles'),
				'singular_name' => __('Mode of Study','dtc-pathways-and-profiles'),
				'hierarchical' => true,
				'post_types' => array ('route')
			),
			'group' => array(
				'name' => __('Studentship Groups','dtc-pathways-and-profiles'),
				'singular_name' => __('Studentship Group','dtc-pathways-and-profiles'),
				'hierarchical' => true,
				'post_types' => array ('studentship')
			),
			'topic' => array(
				'name' => __('Research Keywords','dtc-pathways-and-profiles'),
				'singular_name' => __('Research Keyword','dtc-pathways-and-profiles'),
				'hierarchical' => false,
				'post_types' => array ('profile')
			)
		);
	}

	/*
	 * Define the custom post types to be registered
	 *
	 * @return array array containing the arguments for each new post type, the fields
	 * it should have, and any customisations for display in different contexts
	 */	
	private function post_type_details() {
		$classes[] = 'highlight';	// array to use for route customisation
		if (get_option( 'wdtc_setting_show_pathway_description_after_routes' ))
			$classes[] = 'third-box';
		$studentship_type_details = new WDTC_Studentship_Type_Details;
		return array(
			'profile' => array(
				'name'=> __('Student Profiles','dtc-pathways-and-profiles'),
				'singular_name'=> __('Student Profile','dtc-pathways-and-profiles'),
				'supports'=>array ('title','editor','thumbnail', 'revisions',  'author'),
				'field_data' => array(
					array('WDTC_Custom_Date_Field', 'wdtc-start-date', __('Start date','dtc-pathways-and-profiles'), 'post_meta_data', array('display_format'=>'F Y')),
					array('WDTC_Custom_Date_Field', 'wdtc-end-date', __('End date','dtc-pathways-and-profiles'), 'post_meta_data', array('hidden'=>array('form','display'))),
					array('WDTC_Custom_Text_Field', 'wdtc-research-topic', __('Research Topic','dtc-pathways-and-profiles'), 'post_meta_data', array('validation_pattern'=>'alpha_num_standard')),
					array('WDTC_Custom_Dropdown', 'pathway', __('Research pathway','dtc-pathways-and-profiles'), 'hierarchical_tax_data', array('hidden'=>array('admin'))),
					array('WDTC_Custom_Text_Field', 'wdtc-research-supervisor', __('Research Supervisor','dtc-pathways-and-profiles'), 'post_meta_data', array('validation_pattern'=>'alpha_num_standard')),
					array('WDTC_Custom_Dropdown', 'institution', __('Supervising school','dtc-pathways-and-profiles'), 'hierarchical_tax_data', array('hidden'=>array('admin'))),
					array('WDTC_Custom_Text_Field', 'wdtc-primary-funding-source', __('Primary funding source','dtc-pathways-and-profiles'), 'post_meta_data', array('validation_pattern'=>'alpha_num_standard','default'=>'ESRC Studentship')),
					array('WDTC_Custom_Text_Field', 'wdtc-external-sponsor', __('External Sponsor','dtc-pathways-and-profiles'), 'post_meta_data', array('validation_pattern'=>'alpha_num_standard')),
					array('WDTC_Custom_Text_Field', 'topic', __('Research keywords','dtc-pathways-and-profiles'), 'nonhierarchical_tax_data', array('hidden'=>array('admin'), 'nonhierarchical'=>true)),
					array('WDTC_Custom_Email_Field', 'wdtc-email', __('Email','dtc-pathways-and-profiles'), 'post_meta_data', array('footer_field'=>true)),
					array('WDTC_Custom_URL_Field', 'wdtc-website-url', __('Website','dtc-pathways-and-profiles'), 'post_meta_data', array('footer_field'=>true)),			
					array('WDTC_Custom_URL_Field', 'wdtc-academiaedu-url', __('Academia.edu','dtc-pathways-and-profiles'), 'post_meta_data', array('footer_field'=>true)),			
					array('WDTC_Custom_Username_Field','wdtc-researchgate-username', __('ResearchGate','dtc-pathways-and-profiles'), 'post_meta_data', array('base_url' => 'http://www.researchgate.net/profile', 'validation_pattern'=>'alpha_num_hyphen','footer_field'=>true)),			
					array('WDTC_Custom_Username_Field','wdtc-twitter-username', __('Twitter','dtc-pathways-and-profiles'), 'post_meta_data', array('base_url' => 'http://twitter.com', 'validation_pattern'=>'alpha_num_underscore','usernameprefix'=>'@','footer_field'=>true)),			
					array('WDTC_Custom_URL_Field', 'wdtc-linkedin-url',  __('LinkedIn','dtc-pathways-and-profiles'), 'post_meta_data', array('footer_field'=>true)),		
					array('WDTC_Custom_Username_Field', 'wdtc-facebook-username', __('Facebook','dtc-pathways-and-profiles'), 'post_meta_data', array('base_url' => 'http://www.facebook.com', 'validation_pattern'=>'alpha_num_dot','footer_field'=>true))
				)),
			'route' => array(
				'name'=> __('Pathway Details','dtc-pathways-and-profiles'),
				'singular_name'=> __('Pathway Details','dtc-pathways-and-profiles'),
				'supports'=>array ('revisions'),
				'field_data' => array(
					array('WDTC_Custom_Dropdown', 'institution', __('Institution','dtc-pathways-and-profiles'), 'hierarchical_tax_data' ,array('tax_display'=>'ancestor','hidden'=>array('admin'))),
					array('WDTC_Custom_Dropdown','pathway', __('Pathway','dtc-pathways-and-profiles'), 'hierarchical_tax_data' ,array('tax_display'=>'ancestor','hidden'=>array('admin'))),
					array('WDTC_Custom_Text_Field','wdtc-leader', __('Convener','dtc-pathways-and-profiles'), 'post_meta_data'), 
					array('WDTC_Custom_Text_Field','wdtc-contact', __('Contact','dtc-pathways-and-profiles'), 'post_meta_data'), 
					array('WDTC_Custom_Email_Field','wdtc-email', __('Email','dtc-pathways-and-profiles'), 'post_meta_data'), 
				)),
			'studentship' => array(
				'name'=> __('Studentships','dtc-pathways-and-profiles'),
				'singular_name'=> __('Studentship','dtc-pathways-and-profiles'),
				'supports'=>array ('title', 'revisions'),
				'field_data' => array(
					array('WDTC_Custom_Radio_Buttons', 'wdtc-studentship-type', __('Studentship Type','dtc-pathways-and-profiles'), 'post_meta_data', array('choices' => $studentship_type_details->studentship_type_choices())), 
					array('WDTC_Custom_Dropdown', 'wdtc-studentship-quantity', __('Number of Studentships','dtc-pathways-and-profiles'), 'post_meta_data', array('choices' => range(1,49), 'numerical_up_to'=>true, 'hidden'=>array('display'))), 
					array('WDTC_Custom_Date_Field', 'wdtc-application-date', __('Application Date','dtc-pathways-and-profiles'), 'post_meta_data', array('display_format'=>'j F Y')), 
					array('WDTC_Custom_Dropdown', 'institution', __('Institution','dtc-pathways-and-profiles'), 'hierarchical_tax_data',array('tax_display'=>'ancestor','hidden'=>array('admin'))),
					array('WDTC_Custom_Dropdown', 'pathway', __('Pathway','dtc-pathways-and-profiles'), 'hierarchical_tax_data', array('hidden'=>array('admin'))),
					array('WDTC_Custom_Dropdown', 'group', __('Studentship Group','dtc-pathways-and-profiles'), 'hierarchical_tax_data', array('hidden'=>array('admin'))),
					array('WDTC_Custom_URL_Field', 'wdtc-studentship-url', __('Studentship Details','dtc-pathways-and-profiles'), 'post_meta_data', array('label_as_link'=>true)), 
					array('WDTC_Custom_Date_Field', 'wdtc-interviews-date', __('Week of Interviews','dtc-pathways-and-profiles'), 'post_meta_data'), 
					array('WDTC_Custom_Date_Field', 'wdtc-notification-date', __('Date for Notification of Outcome','dtc-pathways-and-profiles'), 'post_meta_data'),
					array('WDTC_Custom_Text_Field', 'wdtc-studentship-title', __('Project Title','dtc-pathways-and-profiles'), 'post_meta_data'), 
				))
		);			
	}

	/*
	 * Automatically generate the title for specific post types when they are saved.
	 *
	 */
	private function auto_titlers($recipe) {
		/* Special arrangements for setting titles for routes and studentships */
		if (is_admin()) {
			foreach($recipe as $post_type => $taxonomies) {
				$titler = new WDTC_Title_From_Taxonomy_Terms($post_type, $taxonomies);
				add_action( 'save_post', array($titler,'set_title'));
			}
		}
	}

	/*
	 * Set up the plugin's widgets
	 *
	 */
	private function widgets() {
		require_once dirname( __FILE__ ) . '/includes/widgets.php';
		add_action( 'widgets_init', array(&$this, 'init_featured_profile_widget'));
		add_action( 'widgets_init', array(&$this, 'init_studentships_widget'));
	}
	
	function init_featured_profile_widget()
	{
		return register_widget("Featured_Profile_Widget");
	}

	function init_studentships_widget()
	{
		return register_widget("Studentships_Widget");
	}
	
	/*
	 * Set up the plugin's shortcodes
	 *
	 */
	private function shortcodes() {
		require_once dirname( __FILE__ ) . '/includes/shortcodes.php';
		$studentship_lister = new WDTC_Studentship_Lister;
		add_shortcode('studentships', array($studentship_lister, 'handle_shortcode'));
		$profile_displayer = new WDTC_Profile_Displayer;
		add_shortcode('profiles', array($profile_displayer, 'handle_shortcode'));
		$taxonomy_tag_cloud = new WDTC_Taxonomy_Tag_Cloud;
		add_shortcode('tag_cloud', array($taxonomy_tag_cloud, 'handle_shortcode'));
	}
	
	/*
	 * Allow unlimited posts to be displayed for archives
	 *
	 */
	public function no_limit_posts( $query ) {
		if( $query->is_main_query() && !is_admin() && is_archive()) {
			$query->set( 'posts_per_page', '-1' );
		}
	}

	/*
	 * Get the image size for profiles
	 *
	 * @return string image size
	 */
	public function profile_image_size($size) {
		return get_option('wdtc_setting_profile_image_size');
	}

	/*
	 * Get the default image for profiles that don't have a user image
	 *
	 * @return string html for displaying the image
	 */
	public function profile_image_default() {
		return '<img width="75" height="75" src="' . plugins_url( './images/silhouette-75x75.png', __FILE__ ) . '" class="alignleft">';
	}
	
	/*
	 * Display profiles associated with a particular taxonomy at URLs with the
	 * structure /profiles/taxonomy/term eg /profiles/pathway/economics or
	 * profiles/institution/cardiff
	 *
	 * @param WP_Rewrite
	 */
	public function profiles_rewrite_rules( $wp_rewrite ) {
		$new_rules = array(
			'profiles/(.+)/(.+)' => 'index.php?post_type=profile&' . $wp_rewrite->preg_index(1) . '=' . $wp_rewrite->preg_index(2));
		$wp_rewrite->rules = $new_rules + $wp_rewrite->rules;
	}
	
	/*
	 * Display only the route post type if the taxonomy is pathway, i.e.
	 * if the the url is /pathway/term eg /pathway/economics
	 *
	 * @param WP_Rewrite
	 */
	public function routes_rewrite_rules( $wp_rewrite ) {
		$new_rules = array(
			'pathway/(.+)' => 'index.php?pathway=' . $wp_rewrite->preg_index(1)) . '&post_type=route';
		$wp_rewrite->rules = $new_rules + $wp_rewrite->rules;
	}
	
	/*
	 * Set up a form for allowing users to edit their profile
	 *
	 * @param array fields to be edited in the form
	 */
	public function setup_profile_form($profile_fields) {
		if (class_exists('WDTC_Edit_Post_Form')) {
			require_once dirname(__FILE__) . '/includes/forms.php';
			$edit_profile_form = new WDTC_Edit_Profile_Form($profile_fields);
			$edit_profile_form->init();
		}
	}
	
	/*
	 * Set up fields on user profile to match to profile post
	 *
	 */
	private function setup_profile_matcher() {
		require_once dirname( __FILE__ ) . '/includes/profile_matcher.php';
		$matcher = new WDTC_Profile_Matcher;
		$matcher->init();
	}
}

class WDTC_Studentship_Type_Details {

	public function studentship_type_name($type_slug, $quantity = 1, $lowercase = false) {
		$choices = $this->studentship_type_choices_with_number($quantity);
		$type_name = $choices[$type_slug];
		if ($lowercase)
			$type_name = strtolower($type_name);
		return $type_name;
	}

	public function studentship_type_choices() {
		return $this->studentship_type_choices_with_number(1);
	}

	public function studentship_type_choices_with_number($number) {
		return array('open' =>  _n('Open studentship','Open studentships',$number,'dtc-pathways-and-profiles'), 'named' =>  _n('Named studentship','Named studentships',$number,'dtc-pathways-and-profiles'));
	}
}
?>