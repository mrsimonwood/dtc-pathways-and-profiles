# README #

### What is this repository for? ###

* Wordpress Plugin to provides post types, fields and taxonomies relevant to a Doctoral Training Partnership . 
* Version 0.1.17

### How do I get set up? ###

* Depends on the DTP Posts and Fields plugin, requires DTP Frontend Editor to edit profiles.
* Upload to wp-content/plugins via FTP or Plugins > Add New > Upload Plugin
* In the dashboard choose "Activate" or "Activate Plugin"

